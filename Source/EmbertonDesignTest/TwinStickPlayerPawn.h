// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TwinStickPlayerPawn.generated.h"

class ATwinStickGameState;

UCLASS()
class EMBERTONDESIGNTEST_API ATwinStickPlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	ATwinStickPlayerPawn();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FTimerHandle PowerupTimerHandle;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ClampMin = "0.1", ClampMax = "1.0"), Category = "PowerupTimer")
		float PowerupTimerRate;
	UFUNCTION(BlueprintImplementableEvent)
		void OnPowerupTimerTick();

	UFUNCTION()
		void EndGame(AActor* DestroyedActor);
};
