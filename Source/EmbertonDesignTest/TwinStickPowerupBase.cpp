// Fill out your copyright notice in the Description page of Project Settings.


#include "TwinStickPowerupBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATwinStickPowerupBase::ATwinStickPowerupBase()
{
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &ATwinStickPowerupBase::OnOverlap);
}

// Called when the game starts or when spawned
void ATwinStickPowerupBase::BeginPlay()
{
	Super::BeginPlay();

	PlayerPawn = Cast<ATwinStickPlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}

void ATwinStickPowerupBase::OnOverlap(UPrimitiveComponent* OverlappedComponent,
									  AActor* OtherActor,
									  UPrimitiveComponent* OtherComp,
									  int32 OtherBodyIndex,
									  bool bFromSweep,
									  const FHitResult& SweepResult)
{
	if (PlayerPawn == nullptr)
	{
		//DEBUG:
		GEngine->AddOnScreenDebugMessage(1, 5.0, FColor::Magenta, TEXT("Nullptr for PlayerPawn reference in TwinStickPowerupBase.cpp"));

		return;
	}

	if (OtherActor == PlayerPawn)
	{
		OnPowerupPickup();
		Destroy();
	}
}
