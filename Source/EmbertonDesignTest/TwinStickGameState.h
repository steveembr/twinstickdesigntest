// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "TwinStickEnemyPawnBase.h"
#include "TwinStickSpawnPointBase.h"
#include "TwinStickGameState.generated.h"

class ATwinStickEnemyPawnBase;
class ATwinStickSpawnPointBase;

/**
 * 
 */
UCLASS()
class EMBERTONDESIGNTEST_API ATwinStickGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	ATwinStickGameState();

	virtual void HandleBeginPlay() override;

	UFUNCTION()
		void GameOver();

protected:
	APawn* PlayerPawnRef;

	// Overall GameState 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game State")
		bool bIsGameOver;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game State")
		bool bIsGameWon;
	UFUNCTION()
		void ResetGame();
	UFUNCTION()
		void WinGame();

	// Game Timer 
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.1"), Category = "Game Settings")
	float GameLengthInSeconds;
	FTimerHandle GameTimerHandle;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ClampMin = "0.1", ClampMax = "1.0"), Category = "Game Settings")
		float GameTimerRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game State")
		float GameTimeRemaining;

	UFUNCTION()
		void GameTimerTick();

	// Spawn Points
	int RandomSpawnPointIndex;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game State")
		TArray<ATwinStickSpawnPointBase*> EnemySpawnPoints;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game State")
		TArray<ATwinStickSpawnPointBase*> PowerupSpawnPoints;
	UFUNCTION()
		void FindSpawnPoints();

	// Waves to spawn enemies and powerups
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game State")
		int WaveCount;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Game Settings")
		int WaveIntervalForEnemyIncrease;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ClampMin = "1"), Category = "Game Settings")
		int MinEnemiesInWave;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ClampMin = "1"), Category = "Game Settings")
		int MaxEnemiesInWave;
	int EnemiesToSpawnInWave;
	int PowerupsToSpawnInWave;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ClampMin = "0"), Category = "Game Settings")
		int MinPowerupsInWave;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ClampMin = "0"), Category = "Game Settings")
		int MaxPowerupsInWave;

	// Spawn Wave Timer
	FTimerHandle WaveTimerHandle;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.1"), Category = "Wave Settings")
		float WaveTimerRate;
	UFUNCTION()
		void WaveTimerTick();
};
