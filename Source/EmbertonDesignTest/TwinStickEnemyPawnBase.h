// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "GameFramework/PawnMovementComponent.h"
#include "AIController.h"
#include "TwinStickPlayerPawn.h"
#include "TwinStickEnemyPawnBase.generated.h"

class AAIController;
class ATwinStickPlayerPawn;
class UPawnMovementComponent;

/**
 * 
 */
UCLASS()
class EMBERTONDESIGNTEST_API ATwinStickEnemyPawnBase : public ADefaultPawn
{
	GENERATED_BODY()

public:
	ATwinStickEnemyPawnBase();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Debug")
		bool bDrawRangeDebugSpheres;

	UPROPERTY(VisibleAnywhere, Category = "Movement")
		UPawnMovementComponent* PawnMovementComp;

	UPROPERTY()
		AAIController* EnemyAIController;

	UPROPERTY()
		ATwinStickPlayerPawn* PlayerPawn;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		bool bIsPlayerInRange;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0.0"), Category = "Movement")
		float MinDistanceFromPlayer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0.0"), Category = "Movement")
		float MaxDistanceFromPlayer;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0"), Category = "Movement")
		float MoveSpeed;

	UFUNCTION()
		virtual void MoveInPlayerRange(float DeltaTime);
};
