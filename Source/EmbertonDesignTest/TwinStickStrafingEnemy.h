// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TwinStickEnemyPawnBase.h"
#include "TwinStickStrafingEnemy.generated.h"

/**
 * 
 */
UCLASS()
class EMBERTONDESIGNTEST_API ATwinStickStrafingEnemy : public ATwinStickEnemyPawnBase
{
	GENERATED_BODY()
	
public:
	ATwinStickStrafingEnemy();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Strafing movement
	FTimerHandle ChangeDirectionTimerHandle;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0"), Category = "Movement")
		float ChangeDirectionTimerRate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(ClampMin = "0.0", ClampMax= "1.0"), Category = "Movement")
		float ChangeDirectionChance;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		bool bIsStrafingRight;
	UFUNCTION()
		void ChangeDirectionTimerTick();
	UFUNCTION()
		void Strafe(float DeltaTime);

	// Firing
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Firing")
		TSubclassOf<AActor> ProjectileToSpawn;
	UPROPERTY()
		FVector ProjectileSpawnLocation;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Firing")
		float ProjectileSpawnOffset;
	FActorSpawnParameters ProjectileSpawnParams;
	FTimerHandle FireTimerHandle;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0"), Category = "Firing")
		float FireTimerRate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0", ClampMax = "1.0"), Category = "Firing")
		float FireChance;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0"), Category = "Firing")
		float FireDistance;

	UFUNCTION()
		void Fire();
};
