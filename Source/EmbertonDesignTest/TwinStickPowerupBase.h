// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "TwinStickPlayerPawn.h"
#include "TwinStickPowerupBase.generated.h"

class ATwinStickPlayerPawn;
class USphereComponent;

UCLASS()
class EMBERTONDESIGNTEST_API ATwinStickPowerupBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATwinStickPowerupBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		USphereComponent* CollisionComp;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.1"), Category = "Powerup")
		float EffectDuration;

	UPROPERTY(BlueprintReadOnly)
		ATwinStickPlayerPawn* PlayerPawn;

	UFUNCTION()
		void OnOverlap(UPrimitiveComponent* OverlappedComponent,
					   AActor* OtherActor,
					   UPrimitiveComponent* OtherComp,
					   int32 OtherBodyIndex,
					   bool bFromSweep,
					   const FHitResult& SweepResult);

	UFUNCTION(BlueprintImplementableEvent)
		void OnPowerupPickup();
};
