// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "TwinStickEnemyPawnBase.h"
#include "TwinStickPowerupBase.h"
#include "TwinStickSpawnPointBase.generated.h"

/**
 * 
 */
UCLASS()
class EMBERTONDESIGNTEST_API ATwinStickSpawnPointBase : public ATargetPoint
{
	GENERATED_BODY()
public:
	ATwinStickSpawnPointBase();

	UFUNCTION()
		void SpawnRandomEnemy();
	UFUNCTION()
		void SpawnRandomPowerup();

	UPROPERTY(BlueprintReadOnly)
		bool bCanSpawnEnemies;
	UPROPERTY(BlueprintReadOnly)
		bool bCanSpawnPowerups;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int RandomSpawnIndex;

	FVector SpawnLocation;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0"), Category = "Spawn")
		float SpawnRadius;
	UFUNCTION()
		FVector GetRandomSpawnLocation();

	FActorSpawnParameters SpawnParams;

	// Lists of Enemies or Powerups this SpawnPoint is allowed to spawn
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Spawn")
		TArray<TSubclassOf<ATwinStickEnemyPawnBase>> EnemiesToSpawn;
	TSubclassOf<ATwinStickEnemyPawnBase> SelectedEnemyTypeToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Spawn")
		TArray<TSubclassOf<ATwinStickPowerupBase>> PowerupsToSpawn;
	TSubclassOf<ATwinStickPowerupBase> SelectedPowerupTypeToSpawn;
};
