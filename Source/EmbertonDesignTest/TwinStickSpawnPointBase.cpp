// Fill out your copyright notice in the Description page of Project Settings.


#include "TwinStickSpawnPointBase.h"

ATwinStickSpawnPointBase::ATwinStickSpawnPointBase()
{
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
}

// Called when the game starts or when spawned
void ATwinStickSpawnPointBase::BeginPlay()
{
	Super::BeginPlay();

	// Find out what this SpawnPoint can spawn (enemies or powerups)
	bCanSpawnEnemies = false;
	bCanSpawnPowerups = false;
	if (EnemiesToSpawn.Num() > 0)
	{
		bCanSpawnEnemies = true;
	}
	if (PowerupsToSpawn.Num() > 0)
	{
		bCanSpawnPowerups = true;
	}
}

void ATwinStickSpawnPointBase::SpawnRandomEnemy()
{
	if (!bCanSpawnEnemies)
	{
		return;
	}

	RandomSpawnIndex = FMath::RandRange(0, (EnemiesToSpawn.Num() - 1));
	if (EnemiesToSpawn.IsValidIndex(RandomSpawnIndex))
	{
		SelectedEnemyTypeToSpawn = EnemiesToSpawn[RandomSpawnIndex];
		SpawnLocation = GetRandomSpawnLocation();

		GetWorld()->SpawnActor<ATwinStickEnemyPawnBase>(SelectedEnemyTypeToSpawn, SpawnLocation, GetActorRotation(), SpawnParams);
	}
}

void ATwinStickSpawnPointBase::SpawnRandomPowerup()
{
	if (!bCanSpawnPowerups)
	{
		return;
	}
	RandomSpawnIndex = FMath::RandRange(0, (PowerupsToSpawn.Num() - 1));

	if (PowerupsToSpawn.IsValidIndex(RandomSpawnIndex))
	{
		SelectedPowerupTypeToSpawn = PowerupsToSpawn[RandomSpawnIndex];
		SpawnLocation = GetRandomSpawnLocation();

		GetWorld()->SpawnActor<ATwinStickPowerupBase>(SelectedPowerupTypeToSpawn, SpawnLocation, GetActorRotation(), SpawnParams);
	}
}

FVector ATwinStickSpawnPointBase::GetRandomSpawnLocation()
{
	return this->GetActorLocation() + FVector(FMath::RandPointInCircle(SpawnRadius), 0.f);
}