// Fill out your copyright notice in the Description page of Project Settings.

#include "TwinStickPlayerPawn.h"
#include "TwinStickGameState.h"

// Sets default values
ATwinStickPlayerPawn::ATwinStickPlayerPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	PowerupTimerRate = 1.0f;
}

// Called when the game starts or when spawned
void ATwinStickPlayerPawn::BeginPlay()
{
	Super::BeginPlay();

	ATwinStickGameState* GameStateRef = GetWorld()->GetGameState<ATwinStickGameState>();
	if (GameStateRef == nullptr)
	{
		//DEBUG:
		GEngine->AddOnScreenDebugMessage(1, 5.0, FColor::Magenta, TEXT("Nullptr for GameState reference in TwinStickPlayerPawn.cpp"));

		return;
	}

	GetWorld()->GetTimerManager().SetTimer(PowerupTimerHandle, this, &ATwinStickPlayerPawn::OnPowerupTimerTick, PowerupTimerRate, true, PowerupTimerRate);

	OnDestroyed.AddDynamic(this, &ATwinStickPlayerPawn::EndGame);
}

// Called to bind functionality to input
void ATwinStickPlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ATwinStickPlayerPawn::EndGame(AActor* DestroyedActor)
{
	ATwinStickGameState* GameStateRef = GetWorld()->GetGameState<ATwinStickGameState>();
	if (GameStateRef == nullptr)
	{
		//DEBUG:
		GEngine->AddOnScreenDebugMessage(1, 5.0, FColor::Magenta, TEXT("Nullptr for GameState reference in TwinStickPlayerPawn.cpp"));

		return;
	}
	// End the game if the player dies
	GameStateRef->GameOver();
}
