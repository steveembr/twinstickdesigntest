// Fill out your copyright notice in the Description page of Project Settings.


#include "TwinStickEnemyPawnBase.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATwinStickEnemyPawnBase::ATwinStickEnemyPawnBase()
{
	PrimaryActorTick.bCanEverTick = true;

	bUseControllerRotationYaw = true;

	bDrawRangeDebugSpheres = false;
	bIsPlayerInRange = false;

	MinDistanceFromPlayer = 400.f;
	MaxDistanceFromPlayer = 1000.f;
	MoveSpeed = 30.f;

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

void ATwinStickEnemyPawnBase::BeginPlay()
{
	Super::BeginPlay();

	EnemyAIController = Cast<AAIController>(GetController());

	PlayerPawn = Cast<ATwinStickPlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

	PawnMovementComp = this->GetMovementComponent();
	PawnMovementComp->SetPlaneConstraintEnabled(true);
}

// Called every frame
void ATwinStickEnemyPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	MoveInPlayerRange(DeltaTime);
}

void ATwinStickEnemyPawnBase::MoveInPlayerRange(float DeltaTime)
{
	if (PlayerPawn == nullptr)
	{
		//DEBUG:
		GEngine->AddOnScreenDebugMessage(1, 5.0, FColor::Magenta, TEXT("Nullptr for PlayerPawn reference in TwinStickEnemyPawnBase.cpp"));
		
		return;
	}
	if (EnemyAIController == nullptr)
	{
		//DEBUG:
		GEngine->AddOnScreenDebugMessage(1, 5.0, FColor::Magenta, TEXT("Nullptr for EnemyAIController reference in TwinStickEnemyPawnBase.cpp"));

		return;
	}

	// Look at the player
	if (EnemyAIController->GetFocusActor() != PlayerPawn)
	{
		EnemyAIController->SetFocus(PlayerPawn, EAIFocusPriority::Gameplay);
	}

	//DEBUG: Draw spheres for min and max range to player
	if (bDrawRangeDebugSpheres)
	{
		DrawDebugSphere(GetWorld(), this->GetActorLocation(), MaxDistanceFromPlayer, 64, FColor::Red, true, 1.0f, 1, 1.f);
		DrawDebugSphere(GetWorld(), this->GetActorLocation(), MinDistanceFromPlayer, 64, FColor::Green, true, 1.0f, 1, 1.f);
	}

	// Check if we're within the distance range of the player
	bIsPlayerInRange = false;
	float SquaredDistanceToPlayer = GetSquaredDistanceTo(PlayerPawn);
	if (SquaredDistanceToPlayer > (MaxDistanceFromPlayer * MaxDistanceFromPlayer))
	{
		// Move to player
		AddMovementInput(this->GetActorForwardVector(), MoveSpeed * DeltaTime, true);
	}
	else if (SquaredDistanceToPlayer < (MinDistanceFromPlayer * MinDistanceFromPlayer))
	{
		// Move away from player
		AddMovementInput(this->GetActorForwardVector(), -MoveSpeed * DeltaTime, true);
	}
	else
	{
		bIsPlayerInRange = true;
	}
}