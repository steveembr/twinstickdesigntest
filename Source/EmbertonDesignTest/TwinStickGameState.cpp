// Fill out your copyright notice in the Description page of Project Settings.


#include "TwinStickGameState.h"
#include "Kismet/GameplayStatics.h"

ATwinStickGameState::ATwinStickGameState()
{
	// Set general default game settings here. Values that should be reset each game are in ResetGame()
	GameLengthInSeconds = 300.f;
	GameTimerRate = 0.4f;

	WaveTimerRate = 5.f;

	WaveIntervalForEnemyIncrease = 5;
	MinEnemiesInWave = 1;
	MaxEnemiesInWave = 5;

	MinPowerupsInWave = 0;
	MaxPowerupsInWave = 2;
}

void ATwinStickGameState::HandleBeginPlay()
{
	Super::HandleBeginPlay();

	// Set default values for a new game
	ResetGame();
	
	PlayerPawnRef = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	// Logic checks on bounds for Min and Max Enemies and Powerups to spawn per wave
	if (MinEnemiesInWave > MaxEnemiesInWave)
	{
		MinEnemiesInWave = MaxEnemiesInWave;
	}
	if (MinPowerupsInWave > MaxPowerupsInWave)
	{
		MinPowerupsInWave = MaxPowerupsInWave;
	}

	FindSpawnPoints();
}

void ATwinStickGameState::ResetGame()
{
	// Reset game state
	bIsGameOver = false;
	bIsGameWon = false;

	// Reset Wave Count
	WaveCount = 1;

	// Reset and begin GameTimer
	GameTimeRemaining = GameLengthInSeconds;
	GetWorld()->GetTimerManager().SetTimer(GameTimerHandle, this, &ATwinStickGameState::GameTimerTick, GameTimerRate, true, GameTimerRate);

	// Begin WaveTimer
	GetWorld()->GetTimerManager().SetTimer(WaveTimerHandle, this, &ATwinStickGameState::WaveTimerTick, WaveTimerRate, true, 0.f);
}

void ATwinStickGameState::FindSpawnPoints()
{
	// Find any SpawnPoints in the world and populate the appropriate lists
	TArray<AActor*> AllSpawnPoints;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATwinStickSpawnPointBase::StaticClass(), AllSpawnPoints);

	ATwinStickSpawnPointBase* CurrentSpawnPoint;

	for (AActor* SpawnPointActor : AllSpawnPoints)
	{
		// Explicitly cast all found Actors to ATwinStickSpawnPointBase
		CurrentSpawnPoint = Cast<ATwinStickSpawnPointBase>(SpawnPointActor);
		if (CurrentSpawnPoint)
		{
			// If the cast succeeds, check if the SpawnPoint can spawn enemies or powerups and add it to the appropriate lists
			if (CurrentSpawnPoint->bCanSpawnEnemies)
			{
				EnemySpawnPoints.Add(CurrentSpawnPoint);
			}
			if (CurrentSpawnPoint->bCanSpawnPowerups)
			{
				PowerupSpawnPoints.Add(CurrentSpawnPoint);
			}
		}
	}
}

void ATwinStickGameState::GameTimerTick()
{
	// Decrement GameTimeRemaining by GameTimerRate and set it to 0 if the result is < 0
	GameTimeRemaining = (GameTimeRemaining - GameTimerRate >= 0.f) ? GameTimeRemaining - GameTimerRate : 0.f;

	// If time is up, the player has survived and won the game
	if (GameTimeRemaining <= 0.f)
	{
		WinGame();
	}
}

void ATwinStickGameState::WaveTimerTick()
{
	WaveCount++;

	// Find random number of enemies and powerups to spawn in the wave
	EnemiesToSpawnInWave = FMath::RandRange(MinEnemiesInWave, MaxEnemiesInWave);
	
	// Number of enemies to spawn increases as WaveCount increases to scale difficulty
	EnemiesToSpawnInWave += int(WaveCount / WaveIntervalForEnemyIncrease);
	for (int i = 0; i < EnemiesToSpawnInWave; i++)
	{
		RandomSpawnPointIndex = FMath::RandRange(0, (EnemySpawnPoints.Num() - 1));
		if (EnemySpawnPoints.IsValidIndex(RandomSpawnPointIndex))
		{
			EnemySpawnPoints[RandomSpawnPointIndex]->SpawnRandomEnemy();
		}
	}

	PowerupsToSpawnInWave = FMath::RandRange(MinPowerupsInWave, MaxPowerupsInWave);
	for (int i = 0; i < PowerupsToSpawnInWave; i++)
	{
		RandomSpawnPointIndex = FMath::RandRange(0, (PowerupSpawnPoints.Num() - 1));
		if (PowerupSpawnPoints.IsValidIndex(RandomSpawnPointIndex))
		{
			PowerupSpawnPoints[RandomSpawnPointIndex]->SpawnRandomPowerup();
		}
	}
}

void ATwinStickGameState::WinGame()
{
	bIsGameWon = true;

	GameOver();
}

void ATwinStickGameState::GameOver()
{
	bIsGameOver = true;

	GetWorldTimerManager().ClearTimer(GameTimerHandle);
	GetWorldTimerManager().ClearTimer(WaveTimerHandle);

	// Destroy all Enemies
	TArray<AActor*> AllEnemies;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATwinStickEnemyPawnBase::StaticClass(), AllEnemies);
	ATwinStickEnemyPawnBase* CurrentEnemy;
	for (AActor* EnemyActor : AllEnemies)
	{
		// Explicitly cast all found Actors to ATwinStickSpawnPointBase
		CurrentEnemy = Cast<ATwinStickEnemyPawnBase>(EnemyActor);
		if (CurrentEnemy)
		{
			CurrentEnemy->Destroy();
		}
	}

	// Remove player input
	if (PlayerPawnRef)
	{
		PlayerPawnRef->SetActorTickEnabled(false);
	}
}