// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	MaxHealth = 5;

	SetComponentTickEnabled(false);
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = MaxHealth;

	MyOwner = GetOwner();
	if (MyOwner == nullptr)
	{
		//DEBUG:
		GEngine->AddOnScreenDebugMessage(1, 5.0, FColor::Magenta, TEXT("HealthComponent has invalid Owner"));

		return;
	}

	MyOwner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::HandleTakeAnyDamage);
}

void UHealthComponent::HandleTakeAnyDamage(AActor* DamagedActor,
											 float Damage,
											 const class UDamageType* DamageType,
											 class AController* InstigatedBy,
											 AActor* DamageCauser)
{
	CurrentHealth -= Damage;

	if (CurrentHealth <= 0)
	{
		CurrentHealth = 0;
		Die();
	}
}

void UHealthComponent::Die()
{
	//	Spawn death particle effect if one exists
	if (DeathEffect)
	{
		FVector DeathEffectLocation = MyOwner->GetActorLocation();

		FTransform* SpawnTransform = new FTransform(DeathEffectLocation);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DeathEffect, *SpawnTransform, true, EPSCPoolMethod::None, true);
	}

	// Remove the dead Actor
	MyOwner->Destroy();
}
