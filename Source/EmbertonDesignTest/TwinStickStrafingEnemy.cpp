// Fill out your copyright notice in the Description page of Project Settings.


#include "TwinStickStrafingEnemy.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
ATwinStickStrafingEnemy::ATwinStickStrafingEnemy()
{
	PrimaryActorTick.bCanEverTick = true;

	ChangeDirectionTimerRate = 0.2f;
	ChangeDirectionChance = 0.2f;

	MinDistanceFromPlayer = 400.f;
	MaxDistanceFromPlayer = 1000.f;
	MoveSpeed = 30.f;

	bDrawRangeDebugSpheres = false;

	FireTimerRate = 0.7f;
	FireChance = 0.6f;
	FireDistance = 1100.f;

	ProjectileSpawnOffset = 100.0f;
}

void ATwinStickStrafingEnemy::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(ChangeDirectionTimerHandle, this, &ATwinStickStrafingEnemy::ChangeDirectionTimerTick, ChangeDirectionTimerRate, true);
	GetWorld()->GetTimerManager().SetTimer(FireTimerHandle, this, &ATwinStickStrafingEnemy::Fire, FireTimerRate, true, FireTimerRate);

	// Set up parameters for firing shots
	ProjectileSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
}

void ATwinStickStrafingEnemy::Tick(float DeltaTime)
{
	// Parent TwinStickEnemyPawnBase's Tick handles moving into player range
	Super::Tick(DeltaTime);

	// If a strafing enemy is within range of the player, begin the strafing
	if (bIsPlayerInRange)
	{
		Strafe(DeltaTime);
	}
}

void ATwinStickStrafingEnemy::ChangeDirectionTimerTick()
{
	// See if we should randomly change strafe directions or not
	if (FMath::FRand() < ChangeDirectionChance)
	{
		bIsStrafingRight = !bIsStrafingRight;
	}
}

void ATwinStickStrafingEnemy::Strafe(float DeltaTime)
{
	if (bIsStrafingRight)
	{
		AddMovementInput(this->GetActorRightVector(), MoveSpeed * DeltaTime, true);
	}
	else
	{
		AddMovementInput(this->GetActorRightVector(), -MoveSpeed * DeltaTime, true);
	}
}

void ATwinStickStrafingEnemy::Fire()
{
	if (ProjectileToSpawn == nullptr)
	{
		//DEBUG:
		GEngine->AddOnScreenDebugMessage(1, 5.0, FColor::Magenta, TEXT("Nullptr for Projectile in TwinStickStrafingEnemy.cpp"));

		return;
	}

	// Check if we're within firing range of the player. Game was too hard with infinite range sniper enemies
	float SquaredDistanceToPlayer = GetSquaredDistanceTo(PlayerPawn);
	if (SquaredDistanceToPlayer <= (FireDistance * FireDistance))
	{
		if (FMath::FRand() < FireChance)
		{
			ProjectileSpawnLocation = GetActorLocation() + (GetActorForwardVector() * ProjectileSpawnOffset);
			GetWorld()->SpawnActor<AActor>(ProjectileToSpawn, ProjectileSpawnLocation, GetActorRotation(), ProjectileSpawnParams);
		}
	}
}