// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Particles/ParticleSystem.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class EMBERTONDESIGNTEST_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	AActor* MyOwner;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "1"), Category = "Health")
		int MaxHealth;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
		int CurrentHealth;

	//Particle effect to be played when Owner dies
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health")
		UParticleSystem* DeathEffect;

	UFUNCTION()
		void HandleTakeAnyDamage(AActor* DamagedActor,
								 float Damage,
								 const class UDamageType* DamageType,
								 class AController* InstigatedBy,
								 AActor* DamageCauser);
	UFUNCTION()
		void Die();
};
